# Name sorter

Reads file and prints sorted version.

## Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Maintainers](#maintainers)

## Requirements

Requires `cmake` and `gcc`.

## Usage

Uses Cmake
```sh
# Create build directory and move into it
$ mkdir -p build && cd build
# Generate makefile using Cmake - default is release
$ cmake ../     
# Compile the project
$ make
#Run the compiled binary
$ ./name_sorter
```

## Maintainers

[Hannes Ringblom (@HannesRingblom)](https://gitlab.com/HannesRingblom)
