#include <iostream>
#include <string>
#include <fstream>
#include <vector>

bool compare_names(std::string name1, std::string name2);
void print_names(std::vector<std::string> sorted_names);
std::vector<std::string> sort_names(std::vector<std::string> list_of_names);

const int PRINT_NUMBER = 1000;

int main()
{
    std::string current_name;
    std::vector<std::string> name_collection;
    
    //Reading from file and inserting names to a vector
    std::ifstream nameReader;
    nameReader.open("../src/names.txt");
    while(getline(nameReader, current_name))
    {
        name_collection.push_back(current_name);
    }

    //Calling method to sort the vector
    std::vector<std::string> sorted_names;
    sorted_names = sort_names(name_collection);

    //Print the sorted names
    print_names(sorted_names);

    return 0;
}

//Using my version of Bubble-sort
std::vector<std::string> sort_names(std::vector<std::string> list_of_names){

    std::cout<<("Sorting names very efficiently... ")<<std::endl;
    //First for loop determines how many times the vector is iterated through
    for (size_t i = 0; i < list_of_names.size(); i++)
    {
        //Second for loop iterates through the vector comparing two adjecent items at a time.
        for (size_t j = 0; j < list_of_names.size()- i- 1; j++)
        {
            if (!compare_names(list_of_names[j], list_of_names[j+1]))
            {
                //Switching places of the names when needed
                std::string tmp_name = list_of_names[j];
                list_of_names[j] = list_of_names[j+1];
                list_of_names[j+1] = tmp_name;
            }
        }   
    }
    //The list of names is sorted and ready to be returned
    return list_of_names;
}

bool compare_names(std::string name1, std::string name2){

    int shortest_length;
    bool name1_first = false;
    bool first_input_shortest;
    //Check which name is shortest and how long it is
    if (name1.length() <= name2.length())
    {
        shortest_length = name1.length();
        first_input_shortest = true;
    }else{
        shortest_length = name2.length();
        first_input_shortest = false;
    }
    //Iterate through both names until they differ or one of them is at its last letter
    for (size_t i = 0; i < shortest_length; i++)
    {
        if (i == shortest_length - 1 &&  name1[i] == name2[i])
        {
            name1_first = first_input_shortest;
            break;
        }
        if (name1[i] < name2[i])
        {
            name1_first = true;
            break;
        }else if (name1[i] > name2[i])
        {
            name1_first = false;
            break;
        }
    }
    //Returns whether the first name should be first or not
    return name1_first;
}

void print_names(std::vector<std::string> sorted_names){
    std::cout<<"First "<<PRINT_NUMBER<<" sorted names:"<<std::endl;
    for (size_t i = 0; i < PRINT_NUMBER; i++)
    {
        std::cout<<i<<". "<<sorted_names[i]<<std::endl;
    }
    
}